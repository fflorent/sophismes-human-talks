# La valse des arguments moisis


## Présentation de l'auteur
Qui suis-je ? Peu importe !

Je ne suis pas un expert et encore moins une autorité.

C'est douteux ? Tant mieux ! Soyez sceptiques ! <!-- .element: class="fragment" data-fragment-index="1" -->


## Un argument moisi ?

Il y a deux sortes d'arguments moisis :
 - les sophismes (fallacieux, visant à tromper) <!-- .element: class="fragment" data-fragment-index="1" -->
 - les paralogismes (l'auteur est de bonne fois, mais ça reste faux) <!-- .element: class="fragment" data-fragment-index="2" -->

Quoi qu'il en soit, le résultat est le m&ecirc;me : l'argument est faux m&ecirc;me s'il parait rigoureux. <!-- .element: class="fragment" data-fragment-index="3" -->


## Ok Gérard, mais tu veux nous dire quoi ?

Que ces arguments sont présents parmi nous ! Ils manipulent notre esprit et nos jugements !

Non, je ne suis pas un complotiste, mais un promoteur de l'esprit critique ! <!-- .element: class="fragment" data-fragment-index="1" -->


## Ok, mais je m'en fous un peu, c'est un sujet ennuyeux !


